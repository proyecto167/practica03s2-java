package com.example.practica03s2java;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import Modelo.AlumnosDb;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FloatingActionButton fbtnAgregar;
    private FloatingActionButton fbtnCerrar;

    private Aplicacion app;
    private MiAdaptador adaptador;

    private Alumno alumno;
    private int posicion=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Aplicacion app = (Aplicacion) getApplication();
        recyclerView=(RecyclerView) findViewById(R.id.recId);
        adaptador=app.getAdaptador();
        recyclerView.setAdapter(adaptador);
        recyclerView.setHasFixedSize(true); // Mejorar el rendimiento

        // searchView = findViewById(R.id.menu_search);
        // Asignar searchview al toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        fbtnAgregar=(FloatingActionButton) findViewById(R.id.agregarAlumno);
        fbtnCerrar=(FloatingActionButton) findViewById(R.id.cerrarApp);

        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        fbtnAgregar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                alumno=null;
                Intent intent = new Intent(MainActivity.this, AlumnoAlta.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("alumno",alumno);
                bundle.putInt("posicion",posicion);
                intent.putExtras(bundle);

                startActivityForResult(intent,0);
            }
        });

        fbtnCerrar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                AlertDialog.Builder confirmar = new AlertDialog.Builder(MainActivity.this);
                confirmar.setTitle("Salir de la app");
                confirmar.setMessage("¿Desea cerrar la app?");
                confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finishAffinity();
                    }
                });
                confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Cancelar, no se realiza ninguna acción
                    }
                });
                confirmar.show();
            }
        });

        app.getAdaptador().setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                posicion=recyclerView.getChildAdapterPosition(v);
                alumno=app.getAlumnos().get(posicion);

                Intent intent = new Intent(MainActivity.this, AlumnoAlta.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("alumno",alumno);
                bundle.putInt("posicion",posicion);
                intent.putExtras(bundle);

                startActivityForResult(intent,1);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchview, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //adaptador.filtrar(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adaptador.filtrar(newText);
                return true;
            }
        });

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode, resultCode, intent);
        recyclerView.getAdapter().notifyDataSetChanged();

    }

}