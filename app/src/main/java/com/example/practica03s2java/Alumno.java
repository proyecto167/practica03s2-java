package com.example.practica03s2java;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class Alumno implements Serializable {
    private int id;
    private String Carrera;
    private String Matricula;
    private String Nombre;
    private String imageId;

    public Alumno() {

    }

    public Alumno(String matricula, String nombre, String imageId, String carrera){
        this.Matricula = matricula;
        this.Nombre = nombre;
        this.imageId =imageId;
        this.Carrera=carrera;
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public String getTextCarrera() { return Carrera; }
    public void setTextCarrera(String Carrera) { this.Carrera = Carrera; }
    public String getTextMatricula() { return Matricula; }
    public void setTextMatricula(String text2) { this.Matricula = text2; }
    public String getTextNombre(){ return Nombre; }
    public void setTextNombre(String text){
        this.Nombre = text;
    }
    public String getImageId(){
        return imageId;
    }
    public void setImageId(String imageId){
        this.imageId = imageId;
    }

    public static ArrayList<Alumno> llenarAlumnos(){
        ArrayList<Alumno> alumnos=new ArrayList<>();
        return alumnos;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Alumno alumno = (Alumno) o;
        return id == alumno.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
